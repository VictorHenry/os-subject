1. ¿Qué es un repositorio?

· Un repositorio es un archivo en el cual puedes almacenar todo tipo de información.

2. ¿Qué es una rama?

· Una rama es una de las tantas opciones que hay en un repositorio.

3. ¿Son origin y local dos repositorios?

· No, no son repositorios. Cuando clonas un repositorio,deberías ver tanto origin como local que son los nombres que Git le da por defecto al servidor que has clonado. 

4. ¿Qué es .gitignore?

· Es un archivo que sirve para decirle a Git qué archivos o directorios debe ignorar y no subir al repositorio.

5. Busca 5 ejemplos de .gitignore en repositorios de gitlab.

· $ git add .gitignore
  $ git commit -m
  $ git status

· $ mkdir results
  $ touch a.txt b.txt
  $ git status
  $ nano .gitignore
  $ cat .gitignore

· $ touch 123.txt 456.txt 789.txt
  $ 456.txt .gitignore
  $ git status

· $ touch hola.txt adios.txt foto.png chema.html
  $ *.txt .gitignore
  $ git status

· $ touch a.a a.e a.i a.o a.u 
  $ *.[aiu] .gitignore
  $ git status

6. Describe los siguientes comandos:

- status: muestra los ficheros modificados.
- add: añade un fichero al repositorio. 
- commit: cuando quieras subir un archivo, modificado o no, debes usar el commit y poner un título al archivo para que otras personas puedan verlo. 
- clone: clona cualquier URL en GitLab.
- push: para subir directamente al repositorio. 
- pull: para ver y bajar los cambios que se han efectuado de un equipo remoto a uno local. 
- checkout: 
- branch: 
- log: 
- merge: 

7. Explica git add -u

· Este comando sirve para actualizar o para poner al día un archivo que elijas y añadirlo a  GitLab. Sino indicas ese archivo con "-u"por defecto pondrá "." y ese comando significaría que añade todas las actualizaciones pero de todos los archivos modificados. 

